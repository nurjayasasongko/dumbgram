import React from 'react';
import { useState } from 'react';

import Login from '../components/Login';
import Register from '../components/Register';

import style from './LandingPage.module.css';

function LandingPage() {
  const [modalShow, setModalShow] = useState(false);
  const [modalRegisterShow, setModalRegisterShow] = useState(false);
  const toRegister = () => {
    setModalRegisterShow(true);
    setModalShow(false);
  };
  const toLogin = () => {
    setModalShow(true);
    setModalRegisterShow(false);
  };
  return (
    <div className={'body'} style={{ padding: '5rem 7rem', display: 'flex' }}>
      <div className={style.left}>
        <h1 className={style.title}>DumbGram</h1>
        <h2 className={style.tagline}>
          Share your best
          <br />
          photos or videos
        </h2>
        <p className={style.text}>
          Join now, share your creations with another
          <br />
          people and enjoy other creations.
        </p>
        <div className="flexBtn">
          <button
            onClick={() => setModalShow(true)}
            className="btnRainbow"
            style={{ padding: '7px 2.6rem', marginRight: '1rem', color: 'white' }}
          >
            Login
          </button>
          <button
            onClick={() => setModalRegisterShow(true)}
            className={style.register}
          >
            Register
          </button>
        </div>
      </div>
      <div className={style.right}>
        <div className={style.first}>
          <img className={style.image} src='/img/Rectangle 6.png' alt="img1" />
          <img className={style.image} src="/img/Rectangle 5.png" alt="" />
          <img className={style.image} src="/img/Rectangle 10.png" alt="" />
        </div>
        <div className={style.second}>
          <img className={style.image} src="/img/Rectangle 3.png" alt="" />
          <img className={style.image} src="/img/Rectangle 9.png" alt="" />
        </div>
        <div className={style.third}>
          <img className={style.image} src="/img/Rectangle 4.png" alt="" />
          <img className={style.image} src="/img/Rectangle 8.png" alt="" />
          <img className={style.image} src="/img/Rectangle 12.png" alt="" />
        </div>
      </div>

      <Login
        show={modalShow}
        onHide={() => setModalShow(false)}
        here={() => toRegister()}
      />
      <Register
        show={modalRegisterShow}
        onHide={() => setModalRegisterShow(false)}
        here={() => toLogin()}
      />
    </div>
  );
}

export default LandingPage;
