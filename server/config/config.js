require("dotenv").config();
const path = require("path");

const {
  MYSQL_USERNAME,
  MYSQL_PASSWORD,
  MYSQL_DBNAME,
  MYSQL_HOSTNAME,
  MYSQL_PORT
} = process.env;

module.exports = {
  development: {
    username: MYSQL_USERNAME,
    password: MYSQL_PASSWORD,
    database: MYSQL_DBNAME,
    host: MYSQL_HOSTNAME,
    port: MYSQL_PORT,
    dialect: "mysql",
    logging: console.log
  },
  test: {
    username: "root",
    password: "root",
    storage: path.join(__dirname, "../..", "data.sqlite"),
    host: "localhost",
    dialect: "sqlite",
    logging: console.log
  },
  production: {
    username: MYSQL_USERNAME,
    password: MYSQL_PASSWORD,
    database: MYSQL_DBNAME,
    host: MYSQL_HOSTNAME,
    port: MYSQL_PORT,
    dialect: "mysql",
    logging: console.log
  }
}